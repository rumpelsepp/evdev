package main

/*
#include <linux/input.h>

static unsigned int _EVIOCGNAME(int len) {return EVIOCGNAME(len);}
static unsigned int _EVIOCGPHYS(int len) {return EVIOCGPHYS(len);}
static unsigned int _EVIOCGUNIQ(int len) {return EVIOCGUNIQ(len);}
static unsigned int _EVIOCGPROP(int len) {return EVIOCGPROP(len);}

static unsigned int _EVIOCGKEY(int len) {return EVIOCGKEY(len);}
static unsigned int _EVIOCGLED(int len) {return EVIOCGLED(len);}
static unsigned int _EVIOCGSND(int len) {return EVIOCGSND(len);}
static unsigned int _EVIOCGSW(int len)  {return EVIOCGSW(len);}

static unsigned int _EVIOCGBIT(int ev, int len) {return EVIOCGBIT(ev, len);}
static unsigned int _EVIOCGABS(int abs)    {return EVIOCGABS(abs);}
static unsigned int _EVIOCSABS(int abs)    {return EVIOCSABS(abs);}
*/
import "C"
import "fmt"

const size = 255

func main() {
	fmt.Printf("EVIOCGID:         0x%x\n", C.EVIOCGID)
	fmt.Printf("EVIOCGVERSION:    0x%x\n", C.EVIOCGVERSION)
	fmt.Printf("EVIOCGREP:        0x%x\n", C.EVIOCGREP)
	fmt.Printf("EVIOCSREP:        0x%x\n", C.EVIOCSREP)
	fmt.Printf("EVIOCGKEYCODE:    0x%x\n", C.EVIOCGKEYCODE)
	fmt.Printf("EVIOCGKEYCODE_V2: 0x%x\n", C.EVIOCGKEYCODE_V2)
	fmt.Printf("EVIOCSKEYCODE:    0x%x\n", C.EVIOCSKEYCODE)
	fmt.Printf("EVIOCSKEYCODE_V2: 0x%x\n", C.EVIOCSKEYCODE_V2)
	fmt.Printf("EVIOCSFF:         0x%x\n", C.EVIOCSFF)
	fmt.Printf("EVIOCRMFF:        0x%x\n", C.EVIOCRMFF)
	fmt.Printf("EVIOCGEFFECTS:    0x%x\n", C.EVIOCGEFFECTS)
	fmt.Printf("EVIOCGRAB:        0x%x\n", C.EVIOCGRAB)
	fmt.Printf("EVIOCREVOKE:      0x%x\n", C.EVIOCREVOKE)
	fmt.Printf("EVIOCSCLOCKID:    0x%x\n", C.EVIOCSCLOCKID)

	fmt.Printf("EVIOCGNAME:       0x%x\n", C._EVIOCGNAME(size))
	fmt.Printf("EVIOCGPHYS:       0x%x\n", C._EVIOCGPHYS(size))
	fmt.Printf("EVIOCGUNIQ:       0x%x\n", C._EVIOCGUNIQ(size))
	fmt.Printf("EVIOCGPROP:       0x%x\n", C._EVIOCGPROP(size))

	fmt.Printf("EVIOCGKEY:        0x%x\n", C._EVIOCGKEY(size))
	fmt.Printf("EVIOCGLED:        0x%x\n", C._EVIOCGLED(size))
	fmt.Printf("EVIOCGSND:        0x%x\n", C._EVIOCGSND(size))
	fmt.Printf("EVIOCGSW:         0x%x\n", C._EVIOCGSW(size))

	fmt.Printf("EVIOCGBIT:        0x%x\n", C._EVIOCGBIT(0, size))
	fmt.Printf("EVIOCGABS:        0x%x\n", C._EVIOCGABS(size))
	fmt.Printf("EVIOCSABS:        0x%x\n", C._EVIOCSABS(size))
}

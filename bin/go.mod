module git.sr.ht/~rumpelsepp/evdev/bin

go 1.13

require git.sr.ht/~rumpelsepp/evdev v0.0.0-00010101000000-000000000000

replace git.sr.ht/~rumpelsepp/evdev => ../

// +build linux

package evdev

import (
	"encoding/binary"
	"fmt"
	"os"
	"path/filepath"
	"strings"
	"unsafe"

	"golang.org/x/sys/unix"
)

// A Linux input device from which events can be read.
type InputDevice struct {
	Fn string // path to input device (devnode)

	Name string   // device name
	Phys string   // physical topology of device
	File *os.File // an open file handle to the input device
	Fd   int

	Bustype uint16 // bus type identifier
	Vendor  uint16 // vendor identifier
	Product uint16 // product identifier
	Version uint16 // version identifier

	EvdevVersion int // evdev protocol version

	Capabilities     map[CapabilityType][]CapabilityCode // supported event types and codes.
	CapabilitiesFlat map[int][]int
}

// Open an evdev input device.
func OpenDevice(devnode string) (*InputDevice, error) {
	// TODO: check perm
	fd, err := unix.Open(devnode, os.O_RDWR, 0)
	if err != nil {
		return nil, err
	}

	if err := unix.SetNonblock(fd, true); err != nil {
		return nil, err
	}

	dev := InputDevice{
		Fn:   devnode,
		File: os.NewFile(uintptr(fd), devnode),
		Fd:   fd,
	}

	err = dev.set_device_info()
	if err != nil {
		return nil, err
	}

	return &dev, nil
}

func (dev *InputDevice) ReadEvent() (*InputEvent, error) {
	event := InputEvent{}
	if err := binary.Read(dev.File, binary.LittleEndian, &event); err != nil {
		return &event, err
	}
	return &event, nil
}

// Get a useful description for an input device. Example:
//   InputDevice /dev/input/event3 (fd 3)
//     name Logitech USB Laser Mouse
//     phys usb-0000:00:12.0-2/input0
//     bus 0x3, vendor 0x46d, product 0xc069, version 0x110
//     events EV_KEY 1, EV_SYN 0, EV_REL 2, EV_MSC 4
func (dev *InputDevice) String() string {
	evtypes := make([]string, 0)

	for ev := range dev.Capabilities {
		evtypes = append(evtypes, fmt.Sprintf("%s %d", ev.Name, ev.Type))
	}
	evtypes_s := strings.Join(evtypes, ", ")

	return fmt.Sprintf(
		"InputDevice %s (fd %d)\n"+
			"  name %s\n"+
			"  phys %s\n"+
			"  bus 0x%04x, vendor 0x%04x, product 0x%04x, version 0x%04x\n"+
			"  events %s",
		dev.Fn, dev.Fd, dev.Name, dev.Phys, dev.Bustype,
		dev.Vendor, dev.Product, dev.Version, evtypes_s)
}

// An all-in-one function for describing an input device.
func (dev *InputDevice) set_device_info() error {
	info := device_info{}

	err := ioctl(dev.Fd, _EVIOCGID, unsafe.Pointer(&info))
	if err != nil {
		return err
	}

	name, err := eviocgname(dev.Fd)
	if err != nil {
		return err
	}

	// it's ok if the topology info is not available
	phys, _ := eviocgphys(dev.Fd)

	dev.Name = name
	dev.Phys = phys

	dev.Vendor = info.vendor
	dev.Bustype = info.bustype
	dev.Product = info.product
	dev.Version = info.version

	ev_version, err := eviocgversion(dev.Fd)
	if err != nil {
		return err
	}
	dev.EvdevVersion = ev_version

	return nil
}

// Get repeat rate as a two element array.
//   [0] repeat rate in characters per second
//   [1] amount of time that a key must be depressed before it will start
//       to repeat (in milliseconds)
func (dev *InputDevice) GetRepeatRate() *[2]uint {
	repeat_delay := new([2]uint)
	ioctl(dev.Fd, _EVIOCGREP, unsafe.Pointer(repeat_delay))

	return repeat_delay
}

// Set repeat rate and delay.
func (dev *InputDevice) SetRepeatRate(repeat, delay uint) {
	repeat_delay := new([2]uint)
	repeat_delay[0], repeat_delay[1] = repeat, delay
	ioctl(dev.Fd, _EVIOCSREP, unsafe.Pointer(repeat_delay))
}

// Grab the input device exclusively.
func (dev *InputDevice) Grab() error {
	return eviocgrab(dev.Fd, true)
}

// Release a grabbed input device.
func (dev *InputDevice) Release() error {
	return eviocgrab(dev.Fd, false)
}

type CapabilityType struct {
	Type int
	Name string
}

type CapabilityCode struct {
	Code int
	Name string
}

type AbsInfo struct {
	value      int32
	minimum    int32
	maximum    int32
	fuzz       int32
	flat       int32
	resolution int32
}

// Corresponds to the input_id struct.
type device_info struct {
	bustype, vendor, product, version uint16
}

// Determine if a path exist and is a character input device.
func IsInputDevice(path string) bool {
	fi, err := os.Stat(path)

	if os.IsNotExist(err) {
		return false
	}

	m := fi.Mode()
	if m&os.ModeCharDevice == 0 {
		return false
	}

	return true
}

// Return a list of accessible input device names matched by
// deviceglob (default '/dev/input/event*').
func ListInputDevicePaths(device_glob string) ([]string, error) {
	paths, err := filepath.Glob(device_glob)
	if err != nil {
		return nil, err
	}

	devices := make([]string, 0, len(paths))
	for _, path := range paths {
		if IsInputDevice(path) {
			devices = append(devices, path)
		}
	}

	return devices, nil
}

// Return a list of accessible input devices matched by deviceglob
// (default '/dev/input/event/*').
func ListInputDevices(device_glob_arg string, strict bool) ([]*InputDevice, error) {
	device_glob := "/dev/input/event*"
	if device_glob_arg == "" {
		device_glob = device_glob_arg
	}

	fns, err := ListInputDevicePaths(device_glob)
	if err != nil {
		return nil, err
	}

	devices := make([]*InputDevice, 0, len(fns))

	for _, p := range fns {
		dev, err := OpenDevice(p)
		if err != nil && strict {
			return nil, fmt.Errorf("could not open '%s': %w", p, err)
		}
		if err == nil {
			devices = append(devices, dev)
		}
	}

	return devices, nil
}

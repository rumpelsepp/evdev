package evdev

import (
	"bytes"
	"unsafe"

	"golang.org/x/sys/unix"
)

const MAX_NAME_SIZE = 256

func ioctl(fd, name int, data unsafe.Pointer) error {
	_, _, err := unix.Syscall(unix.SYS_IOCTL, uintptr(fd), uintptr(name), uintptr(data))
	if err == 0 {
		return nil
	}
	return err
}

func ioctlGetString(fd, name int) (string, error) {
	rawString := new([MAX_NAME_SIZE]byte)
	err := ioctl(fd, name, unsafe.Pointer(rawString))
	if err != nil {
		return "", err
	}

	length := bytes.IndexByte(rawString[:], 0)
	buf := make([]byte, length)
	copy(buf, rawString[:length])
	return string(buf), nil
}

// get driver version
func eviocgversion(fd int) (int, error) {
	return unix.IoctlGetInt(fd, _EVIOCGVERSION)
}

func eviocgname(fd int) (string, error) {
	return ioctlGetString(fd, _EVIOCGNAME)
}

func eviocgphys(fd int) (string, error) {
	return ioctlGetString(fd, _EVIOCGPHYS)
}

func eviocgrab(fd int, grab bool) error {
	if grab {
		d := int(1)
		if err := ioctl(fd, _EVIOCGRAB, unsafe.Pointer(&d)); err != nil {
			return err
		}
		return nil
	}
	d := int(0)
	if err := ioctl(fd, _EVIOCGRAB, unsafe.Pointer(&d)); err != nil {
		return err
	}
	return nil
}

func eviocgbit(fd, ev int) ([]byte, error) {
	bits := new([(EV_MAX + 1) / 8]byte)

	err := ioctl(fd, _EVIOCGBIT+ev, unsafe.Pointer(bits))
	if err != nil {
		return nil, err
	}

	buf := make([]byte, len(bits))
	copy(buf, bits[:])
	return buf, nil
}

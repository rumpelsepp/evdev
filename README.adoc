# evdev

This package provides Go language bindings to the generic input event
interface in Linux. The *evdev* interface serves the purpose of
passing events generated in the kernel directly to userspace through
character devices that are typically located in `/dev/input/`.

Git Repository: http://git.sr.ht/~rumpelsepp/evdev
